/*
 * grunt-include-replace
 * https://github.com/alanshaw/grunt-include-replace
 *
 * Copyright (c) 2013 Alan Shaw
 * Licensed under the MIT license.
 */

module.exports = function(grunt) {

	'use strict';

	var _ = grunt.util._;
	var path = require('path');

	grunt.registerMultiTask('includereplace', 'Include files and replace variables and evaluate conditions', function() {

		var options = this.options({
			prefix: '<!-- @@',
			suffix: ' -->',
			globals: {},
			includesDir: '',
			docroot: '.'
		});

		grunt.log.debug('Options', options);

		// Variables available in ALL files
		var globalVars = options.globals;

		// Names of our variables
		var globalVarNames = Object.keys(globalVars);

		globalVarNames.forEach(function(globalVarName) {
			if (_.isString(globalVars[globalVarName])) {
				globalVars[globalVarName] = globalVars[globalVarName];
			} else {
				globalVars[globalVarName] = JSON.stringify(globalVars[globalVarName]);
			}
		});

		// Cached variable regular expressions
		var globalVarRegExps = {};

		function ifelse(src) {

			var separate = function (matches) {

				var seperated = [];

				/*
				 loop through each statement and store each section of the statement for use.
				 */

				for (var i = 0; i < matches.length; i++) {

					if(matches[i]){

						var prefix = new RegExp(options.prefix, 'g'), suffix = new RegExp(options.suffix, 'g')

						var match = matches[i].replace(prefix, '').replace(suffix, '');

						var statement = {};

						//type of comparison
						statement.type = match.match(/(elseif|else|if)/)[0];

						//original statement
						statement.original = matches[i];

						//gets the comparison
						statement.comparison = match.match(/\([^]*\)/)[0].replace('(', '').replace(')', '');

						//gets the content
						statement.content = match.match(/\{[^]*\}/)[0].replace('{', '').replace('}', '');

						seperated.push(statement);

					}

				}

				return seperated

			};

			var validate = function(statementGroups){

				var isError = {error: false, message: 'All conditions verified'};

				for (var i = 0; i < statementGroups.length; i++) {

					var statementGroup = statementGroups[i];

					//check if first statement in array is if
					statementGroup[0].check = statementGroup[0].type === 'if';

					//check if last statement in array is else
					if(statementGroup.length >= 2){

						statementGroup[statementGroup.length -1].check = statementGroup[statementGroup.length -1].type === 'else' || statementGroup[statementGroup.length -1].type === 'elseif';

					}

					//check if the ones between are elseifs
					if(statementGroup.length > 2){

						for (var n = 1; n < statementGroup.length -1; n++) {

							statementGroup[n].check = statementGroup[n].type === 'elseif';

						}

					}

					for (var j = 0; j < statementGroup.length; j++) {

						if(statementGroup[j].check === false){
							isError = {error : true, message : 'error at ' + statementGroup[j].original};
							break;
						}

					}

					statementGroups[i] = statementGroup;

				}

				if(isError.error === true){
					grunt.log.error(isError.message);
					return false
				} else {
					grunt.log.ok(isError.message);
					return statementGroups;
				}

			};

			var group = function(statements){

				var grouped = [], i = -1;

				for (var j = 0; j < statements.length; j++) {

					if(statements[j].type == 'if') i++;

					if(!grouped[i]) grouped[i] = [];

					if(statements[j]) grouped[i].push(statements[j]);

				}

				return grouped;

			};

			var evaluate = function (statements) {

				for (var i = 0; i < statements.length; i++) {

					var statement = statements[i], conditionMet = false;

					for (var j = 0; j < statement.length; j++) {

						var test = eval(statement[j].comparison);

						if(test && conditionMet === false){

							src = src.replace(statement[j].original, statement[j].content);
							conditionMet = true;

						} else {

							src = src.replace(statement[j].original,'');

						}

					}

				}

			};

			var conditions = new RegExp('(' + options.prefix + 'elseif\\s?\\([^]+?\\)\\s?\\{' + options.suffix + '[^]+?(' + options.prefix + '}' + options.suffix + ')|' + options.prefix + 'if\\s?\\([^]+?\\)\\s?\\{' + options.suffix + '[^]+?(' + options.prefix + '}' + options.suffix + ')|' + options.prefix + 'else\\s?\\([^]+?\\)\\s?\\{' + options.suffix + '[^]+?(' + options.prefix + '}' + options.suffix + '))', 'g');

			var statements = src.match(conditions);

			//group the returned statements
			if(statements != null){

				statements = separate(statements);

				statements = group(statements);

				statements = validate(statements);

				evaluate(statements);

			}

			return src;

		}

		function replace(contents, localVars) {

			localVars = localVars || {};

			var varNames = Object.keys(localVars);
			var varRegExps = {};

			// Replace local vars
			varNames.forEach(function(varName) {

				// Process lo-dash templates (for strings) in global variables and JSON.stringify the rest
				if (_.isString(localVars[varName])) {
					localVars[varName] = grunt.template.process(localVars[varName]);
				} else {
					localVars[varName] = JSON.stringify(localVars[varName]);
				}

				varRegExps[varName] = varRegExps[varName] || new RegExp(options.prefix + varName + options.suffix, 'g');

				contents = contents.replace(varRegExps[varName], localVars[varName]);
			});

			// Replace global variables
			globalVarNames.forEach(function(globalVarName) {

				globalVarRegExps[globalVarName] = globalVarRegExps[globalVarName] || new RegExp(options.prefix + globalVarName + options.suffix, 'g');

				contents = contents.replace(globalVarRegExps[globalVarName], globalVars[globalVarName]);
			});

			return contents;
		}

		var includeRegExp = new RegExp(options.prefix + 'include\\(\\s*["\'](.*?)["\'](,\\s*({[\\s\\S]*?})){0,1}\\s*\\)' + options.suffix);

		function include(contents, workingDir) {

			var matches = includeRegExp.exec(contents);

			// Create a function that can be passed to String.replace as the second arg
			function createReplaceFn (replacement) {
				return function () {
					return replacement;
				};
			}

			function getIncludeContents (includePath, localVars) {
				var files = grunt.file.expand(includePath),
					includeContents = '';

				files.forEach(function (filePath, index) {
					includeContents += grunt.file.read(filePath);
					// break a line for every file, except for the last one
					includeContents += index !== files.length-1 ? '\n' : '';

					// Make replacements
					includeContents = replace(includeContents, localVars);

					includeContents = ifelse(includeContents);

					// Process includes
					includeContents = include(includeContents, path.dirname(filePath));
					if (options.processIncludeContents && typeof options.processIncludeContents === 'function') {
						includeContents = options.processIncludeContents(includeContents, localVars);
					}
				});

				return includeContents;
			}

			while (matches) {

				var match = matches[0];
				var includePath = matches[1];
				var localVars = matches[3] ? JSON.parse(matches[3]) : {};

				if (!grunt.file.isPathAbsolute(includePath)) {
					includePath = path.resolve(path.join((options.includesDir ? options.includesDir : workingDir), includePath));
				} else {
					if (options.includesDir) {
						grunt.log.error('includesDir works only with relative paths. Could not apply includesDir to ' + includePath);
					}
					includePath = path.resolve(includePath);
				}

				var docroot = path.relative(path.dirname(includePath), path.resolve(options.docroot)).replace(/\\/g, '/');

				// Set docroot as local var but don't overwrite if the user has specified
				if (localVars.docroot === undefined) {
					localVars.docroot = docroot ? docroot + '/' : '';
				}

				grunt.log.debug('Including', includePath);
				grunt.log.debug('Locals', localVars);

				var includeContents = getIncludeContents(includePath, localVars);
				contents = contents.replace(match, createReplaceFn(includeContents));

				matches = includeRegExp.exec(contents);
			}

			return contents;
		}

		this.files.forEach(function(config) {

			config.src.forEach(function(src) {

				grunt.log.debug('Processing glob ' + src);

				if (!grunt.file.isFile(src)) {
					return;
				}

				grunt.log.debug('Processing ' + src);

				// Read file
				var contents = grunt.file.read(src);

				var docroot = path.relative(path.dirname(src), path.resolve(options.docroot)).replace(/\\/g, '/');
				var localVars = {docroot: docroot ? docroot + '/' : ''};

				grunt.log.debug('Locals', localVars);

				// Make replacements
				contents = replace(contents, localVars);

				// Process includes
				contents = include(contents, path.dirname(src));

				//grunt.log.debug(contents);

				var dest = config.dest;

				if (isDirectory(dest) && !config.orig.cwd) {
					dest = path.join(dest, src);
				}

				grunt.log.debug('Saving to', dest);

				grunt.file.write(dest, contents);

				grunt.log.ok('Processed ' + src);
			});
		});
	});

	// Detect if destination path is a directory
	function isDirectory (dest) {
		return grunt.util._.endsWith(dest, '/');
	}
};
